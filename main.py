import tkinter as tk
from tkinter import BOTTOM
import os, time
from typing import List

if os.path.basename(os.getcwd()) == 'sound':
    from tune import Tuner
    from motors import Motor
    from ACGE import ACGE


def frame_maker(frame, next_frame, background_img, btn_img, btn_location, ):
    frame.frame_image = tk.PhotoImage(file = background_img)
    frame.frame_background = tk.Label(frame, image = frame.frame_image)
    frame.frame_background.place(x=0,y=0)
    frame.btn = tk.PhotoImage(file = btn_img)
    frame.frame_btn=tk.Button(frame, image = frame.btn, borderwidth=0,command=lambda:show_frame(next_frame))
    frame.frame_btn.pack(side=BOTTOM)
    frame.frame_btn.place(y = btn_location[0], x = btn_location[1])
    pass

def show_frame(frame):
    # time.sleep(10)
    frame.tkraise()

window=tk.Tk()
window.title('ukulele')
window.minsize(240,320)
window.maxsize(240,320)
window.rowconfigure(0,weight=1)
window.columnconfigure(0,weight=1)

frames : List[tk.Frame] = []
for i in range(0,3):
    frame = tk.Frame(window)
    frame.grid(row=0,column=0,sticky='nsew')
    frames.append(frame)

txt_log_frame = tk.Frame(window)
txt_log_frame.grid(row=0,column=0,sticky='nsew')
v = tk.StringVar()
tk.Label(txt_log_frame, textvariable=v, anchor='w').pack(fill='both')

v.set('Pack\nPack\nPack\nPack\n')

xs = [\
    (frames[0], frames[1], "./res/SDP_Home.png",          "./res/Button_design.png",      (250,70)),\
    (frames[1], txt_log_frame, "./res/SDP_Main_CHOSEN_1.png", "./res/Button_design.png",      (250,70)), \
    (frames[2], frames[0], "./res/SDP_End.png",           "./res/SDP_End__button.png",    (220,60))]


for x in xs:
    print(*x)
    frame_maker(*x)

show_frame(frames[0])
window.mainloop()
exit()
