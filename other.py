import tkinter as tk
from tkinter import BOTTOM
import os
if os.path.basename(os.getcwd()) == 'sound':
    from tune import Tuner
    from motors import Motor
    from ACGE import ACGE

# motor = Motor()
# tuner = Tuner(motor, ACGE.C)
# tuner.tune(debug=True)


def show_frame(frame):
    frame.tkraise()

def chosen_note(panel,note):
    img = tk.PhotoImage(file = note)
    panel.configure(image = img)
    panel.image = img

window=tk.Tk()
window.title('ukulele')
window.attributes("-fullscreen", True)
#window.state('zoomed')
window.minsize(240,320)
window.maxsize(240,320)

window.rowconfigure(0,weight=1)
window.columnconfigure(0,weight=1)

frame1 = tk.Frame(window)
frame2 = tk.Frame(window)
frame3 = tk.Frame(window)

for frame in (frame1,frame2,frame3):
    frame.grid(row=0,column=0,sticky='nsew')


#frame1 code
frame1_image = tk.PhotoImage(file = "./res/SDP_Home.png")
frame1_background = tk.Label(frame1, image = frame1_image)
frame1_background.place(x=0,y=0)

#frame1_title= tk.Label(frame1,text='frame 1',bg='red')
#frame1_title.pack(fill='x')

btn1 = tk.PhotoImage(file = "./res/Button_design.png")

frame1_btn=tk.Button(frame1,image = btn1, borderwidth=0,command=lambda:show_frame(frame2))
frame1_btn.pack(side=BOTTOM)
frame1_btn.place(y = 250, x = 70)




#frame2 code
frame2_image = tk.PhotoImage(file = "./res/all_strings.png")
frame2_background = tk.Label(frame2, image = frame2_image)
frame2_background.place(x=0,y=0)

g_btn_image = tk.PhotoImage(file = "./res/g_button.png")
g_btn = tk.Button(frame2,image = g_btn_image, command = lambda: chosen_note(frame2_background,"./res/g_string.png"))
g_btn.place(y=70,x=70)
#frame1_title= tk.Label(frame1,text='frame 1',bg='red')
#frame1_title.pack(fill='x')

btn2 = tk.PhotoImage(file = "./res/Button_design.png")

frame2_btn=tk.Button(frame2,image = btn2, borderwidth=0,command=lambda:show_frame(frame3))
frame2_btn.pack(side=BOTTOM)
frame2_btn.place(y = 250, x = 70)


#frame3 code
frame3_image = tk.PhotoImage(file = "./res/SDP_End.png")
frame3_background = tk.Label(frame3, image = frame3_image)
frame3_background.place(x=0,y=0)

#frame1_title= tk.Label(frame1,text='frame 1',bg='red')
#frame1_title.pack(fill='x')

btn3 = tk.PhotoImage(file = "./res/SDP_End__button.png")

frame3_btn=tk.Button(frame3,image = btn3, borderwidth=0,command=lambda:show_frame(frame1))
frame3_btn.pack(side=BOTTOM)
frame3_btn.place(y = 220, x = 60)

show_frame(frame1)

window.mainloop()
